#include "Bishop.h"

//the constructor init the piece object
Bishop::Bishop(int x, int y, bool color) :Piece(x, y, color, TYPE_OF_BISHOP)
{
}

//distrucror
Bishop::~Bishop()
{

}

//the function call the function checkBiasMove to check the move
bool Bishop::checkMove(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)const
{
	return checkDiagonalMove(pieces, dst);
}

//the finction check if there is piece in the way to the destination
bool Bishop::checkDiagonalMove(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)const
{
	int i = 0;
	int xOffset = 1;
	int yOffset = 1;
	int x = this->_x;
	int y = this->_y;
	bool validMove = true;

	if (abs(this->_x - dst.getX()) == abs(this->_y - dst.getY())) //check if the move is on some bias
	{
		if (this->_x > dst.getX()) //check who is in the left side
		{
			xOffset = -1;
		}
		if (this->_y > dst.getY())
		{
			yOffset = -1;
		}
		//check the path
		for (i = 0; i < abs(this->_x - dst.getX()) - 1; i++)
		{
			x += xOffset;
			y += yOffset;
			if (pieces[x][y]->getType() != TYPE_OF_EMPTY) //check if someone on the way
			{
				return false;
			}
		}
	}
	else
	{
		return false;
	}
	return true;
}

