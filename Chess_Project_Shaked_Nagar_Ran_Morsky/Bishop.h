#pragma once
#include "Piece.h"

#define MAIN_BIAS true
#define SECOND_BIAS false

class Bishop :public Piece
{
public:
	//the constructor init the piece object
	Bishop(int x, int y, bool color);
	//distrucror
	~Bishop();
	//the function call the function checkBiasMove to check the move
	virtual bool checkMove(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)const;


protected:
	//the finction check if there is piece in the way to the destination
	bool checkDiagonalMove(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)const;
};

