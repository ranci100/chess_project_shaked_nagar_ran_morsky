#include "Board.h"


Board::Board()
{
	int i = 0;
	int j = 0;

	for (i; i < BOARD_SIZE; i++)
	{
		for (j = 0; j < BOARD_SIZE; j++)
		{
			if (i == BLACK_PAWNS)//check if the piece is one of the white pawns
			{
				this->_pieces[i][j] = new Pawn(j, i, false);
			}
			else if (i == WHITE_PAWNS)//check if the piece is one of the white pawns
			{
				this->_pieces[i][j] = new Pawn(j, i, true);
			}
			else if (i == BLACK_PIECE)// check if the piece is one of the pieces of the black player
			{
				if (j == FIRST_ROOK || j == SECOND_ROOK)
				{
					this->_pieces[i][j] = new Rook(j, i, false);
				}
				else if (j == FIRST_BISHOP || j == SECOND_BISHOP)
				{
					this->_pieces[i][j] = new Bishop(j, i, false);
				}
				else if (j == FIRST_KNIGHT || j == SECOND_KNIGHT)
				{
					this->_pieces[i][j] = new Knight(j, i, false);
				}
				else if (j == KING)
				{
					this->_pieces[i][j] = new King(j, i, false);
					this->_blackKing = this->_pieces[i][j];
				}
				else if (j == QUEEN)
				{
					this->_pieces[i][j] = new Queen(j, i, false);
				}
			}
			else if (i == WHITE_PIECE)// check if the piece is one of the pieces of the white player
			{
				if (j == FIRST_ROOK || j == SECOND_ROOK)
				{
					this->_pieces[i][j] = new Rook(j, i, true);
				}
				else if (j == FIRST_BISHOP || j == SECOND_BISHOP)
				{
					this->_pieces[i][j] = new Bishop(j, i, true);
				}
				else if (j == FIRST_KNIGHT || j == SECOND_KNIGHT)
				{
					this->_pieces[i][j] = new Knight(j, i, true);
				}
				else if (j == KING)
				{
					this->_pieces[i][j] = new King(j, i, true);
					this->_whiteKing = this->_pieces[i][j];
				}
				else if (j == QUEEN)
				{
					this->_pieces[i][j] = new Queen(j, i, true);
				}
			}
			else
			{
				this->_pieces[i][j] = new Empty(j, i, true);
			}
		}
	}
}

/*
	This is the destructor of the board. It frees all the memory that has been taken in the board object
*/
Board::~Board()
{
	unsigned int i = 0;
	unsigned int j = 0;


	for (i = 0; i < BOARD_SIZE; i++)
	{
		for (j = 0; j < BOARD_SIZE; j++)
		{
			delete this->_pieces[i][j];
		}
	}
}

/*
	The function returns pointer to the king with the given color
*/
Piece* Board::getKing(bool color)const 
{
	if (color) 
	{
		return this->_whiteKing;
	}
	else 
	{
		return this->_blackKing;
	}
}



/*
	This function gets 2 pieces and make an eat move in the board
	(take the first piece to the second and replace the first piece with empty)
*/
void Board::eat(Piece& src, Piece& dst)
{
	int srcX = src.getX();
	int srcY = src.getY();

	int dstX = dst.getX();
	int dstY = dst.getY();

	delete this->_pieces[dstY][dstX];

	src.setX(dstX);
	src.setY(dstY);

	

	this->_pieces[dstY][dstX] = this->_pieces[srcY][srcX];

	this->_pieces[srcY][srcX] = new Empty(srcX, srcY, false);

}

/*
	This function gets a Piece and a color
	and returns whether or not the given piece is being threating by a piece of the given color
*/
bool Board::isThreating(Piece& dst, bool color)
{
	unsigned int i = 0;
	unsigned int j = 0;

	bool threating = false;

	for (i = 0; i < BOARD_SIZE && !threating; i++)
	{
		for (j = 0; j < BOARD_SIZE && !threating; j++)
		{
			if (this->_pieces[i][j]->getType() != TYPE_OF_EMPTY && this->_pieces[i][j]->getColor() == color)
			{
				threating = threating || this->_pieces[i][j]->checkMove(this->_pieces, dst);
			}
		}
	}

	return threating;

}


char* Board::toString() const
{
	int i = 0;
	int j = 0;
	char stringOfBoard[PIECEDS_IN_BOARD + 1] = { 0 };
	char firstLetterOfType = '0';

	for (i; i < BOARD_SIZE; i++)
	{
		for (j; j < BOARD_SIZE; j++)
		{

		}
		firstLetterOfType = this->_pieces[i][j]->getType()[0];
		stringOfBoard[i] = firstLetterOfType;
	}
	stringOfBoard[PIECEDS_IN_BOARD] = '\0';
}

void Board::getPlayedPieces(std::string move, Piece** src, Piece** dst)
{
	
	int a, b;
	a = move[1] - '1';
	b = move[0] - 'a';
	*src = (this->_pieces[move[1] - '1'][move[0] - 'a']);
	a = move[3] - '1';
	b = move[2] - 'a';
	*dst = (this->_pieces[move[3] - '1'][move[2] - 'a']);
	
}

void Board::moveHandler(Piece& src, Piece& dst)
{
	

	if (dst.getType() != TYPE_OF_EMPTY && (src.getColor() == dst.getColor()))
	{
		throw InvalidDstException();
	}
	if (!src.checkMove(this->_pieces, dst))
	{
		throw InvalidMovementException();
	}

}


void Board::copy(Board& oldBoard)
{
	unsigned int i = 0;
	unsigned int j = 0;

	
	for (i = 0; i < BOARD_SIZE; i++) 
	{
		for (j = 0; j < BOARD_SIZE; j++) 
		{
			if (oldBoard._pieces[i][j]->getType() == TYPE_OF_EMPTY) 
			{
				this->_pieces[i][j] = new Empty(oldBoard._pieces[i][j]->getX(), oldBoard._pieces[i][j]->getY(), oldBoard._pieces[i][j]->getColor());
			}
			else if (oldBoard._pieces[i][j]->getType() == TYPE_OF_PAWN)
			{
				this->_pieces[i][j] = new Pawn(oldBoard._pieces[i][j]->getX(), oldBoard._pieces[i][j]->getY(), oldBoard._pieces[i][j]->getColor());
			}
			else if (oldBoard._pieces[i][j]->getType() == TYPE_OF_KNIGHT)
			{
				this->_pieces[i][j] = new Knight(oldBoard._pieces[i][j]->getX(), oldBoard._pieces[i][j]->getY(), oldBoard._pieces[i][j]->getColor());
			}
			else if (oldBoard._pieces[i][j]->getType() == TYPE_OF_BISHOP)
			{
				this->_pieces[i][j] = new Bishop(oldBoard._pieces[i][j]->getX(), oldBoard._pieces[i][j]->getY(), oldBoard._pieces[i][j]->getColor());
			}
			else if (oldBoard._pieces[i][j]->getType() == TYPE_OF_ROOK)
			{
				this->_pieces[i][j] = new Rook(oldBoard._pieces[i][j]->getX(), oldBoard._pieces[i][j]->getY(), oldBoard._pieces[i][j]->getColor());
			}
			else if (oldBoard._pieces[i][j]->getType() == TYPE_OF_QUEEN)
			{
				this->_pieces[i][j] = new Queen(oldBoard._pieces[i][j]->getX(), oldBoard._pieces[i][j]->getY(), oldBoard._pieces[i][j]->getColor());
			}
			else if (oldBoard._pieces[i][j]->getType() == TYPE_OF_KING)
			{
				this->_pieces[i][j] = new King(oldBoard._pieces[i][j]->getX(), oldBoard._pieces[i][j]->getY(), oldBoard._pieces[i][j]->getColor());
				if (oldBoard._pieces[i][j]->getColor()) 
				{
					this->_whiteKing = this->_pieces[i][j];
				}
				else 
				{
					this->_blackKing = this->_pieces[i][j];
				}
			}
		}
	}
}
