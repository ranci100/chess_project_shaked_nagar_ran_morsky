#pragma once
#include "Piece.h"
#include "Bishop.h"
#include "King.h"
#include "Empty.h"
#include "Knight.h"
#include "Pawn.h"
#include "Queen.h"
#include "Rook.h"

#define NUM_OF_PIECES 64

#define WHITE_PIECE 7
#define BLACK_PIECE 0
#define FIRST_ROOK 0
#define SECOND_ROOK 7
#define FIRST_KNIGHT 1
#define SECOND_KNIGHT 6
#define FIRST_BISHOP 2
#define SECOND_BISHOP 5
#define QUEEN 3
#define KING 4
#define WHITE_PAWNS 6
#define BLACK_PAWNS 1    

#define PIECEDS_IN_BOARD 64

class Board
{
public:
	//methods
	Board();
	~Board();
	//Piece* getWhiteKing()const;
	//Piece* getBlackKing()const;
	Piece* getKing(bool color)const;
	void enPassent(Piece& src, Piece& dst);
	void eat(Piece& src, Piece& dst);
	bool isThreating(Piece& dst, bool color);
	bool checkEnd()const;
	void getPlayedPieces(std::string move, Piece** src, Piece** dst);
	char* toString()const;

	void moveHandler(Piece& src, Piece& dst);

	Board(Board& oldBoard);
	void copy(Board& oldBoard);

private:
	//methods
	
	//fields
	Piece* _pieces[BOARD_SIZE][BOARD_SIZE];
	Piece* _blackKing;
	Piece* _whiteKing;
};
