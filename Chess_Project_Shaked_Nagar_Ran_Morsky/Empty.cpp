#include "Empty.h"

/*
	This is the constructor of empty
*/
Empty::Empty(int x, int y, bool color) 
	:Piece(x, y, color, TYPE_OF_EMPTY)
{
	
}

/*
	This is the destructor if Empty
*/
Empty::~Empty() 
{
	
}

/*
	This function throw an InvalidSrcException because you can not try to move an empty square
*/
bool Empty::checkMove(Piece* piece[BOARD_SIZE][BOARD_SIZE], Piece& dst)const 
{
	throw InvalidSrcException();
}