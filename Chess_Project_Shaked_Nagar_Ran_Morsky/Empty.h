#include "Piece.h"


class Empty : public Piece
{
public:

	Empty(int x, int y, bool color);

	~Empty();

	virtual bool checkMove(Piece* piece[BOARD_SIZE][BOARD_SIZE], Piece & dst)const;
};



