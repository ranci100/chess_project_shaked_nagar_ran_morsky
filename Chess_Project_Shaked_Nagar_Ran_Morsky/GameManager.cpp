#include "GameManager.h"

GameManager::GameManager()
{
	this->_board;
	this->_turnCounter = 0;
}


GameManager::~GameManager()
{

}

char GameManager::turnHandler(std::string move)
{	

	Piece* src = 0;
	Piece* dst = 0;

	this->_board.getPlayedPieces(move, &src, &dst);

	try 
	{
		if (src == dst)
		{
			throw SrcAndDstAreSameException();
		}

		if (src->getType() == TYPE_OF_EMPTY)
		{
			throw InvalidSrcException();
		}
		if ((src->getColor() && this->_turnCounter % 2 == 0) || (!src->getColor() && this->_turnCounter % 2 == 1))
		{
			throw InvalidSrcException();
		}
		

		this->_board.moveHandler(*src, *dst);

		this->_oldBoard.copy(this->_board);

		this->_board.eat(*src, *dst);
		
		this->_turnCounter++;


		if (this->_board.isThreating(*this->_board.getKing(src->getColor()), !src->getColor()))//means illegal move
		{
			this->_board.copy(this->_oldBoard);//undoing the last movement
			this->_turnCounter--;
			throw UnconventionalMovementException();
		}
		else if(this->_board.isThreating(*this->_board.getKing(!src->getColor()), src->getColor()))//player made chess
		{
			return PLAYER_MADE_CHESS;//to use define
		}
	
		
	}
	catch (const std::exception& e) 
	{
		//returnString[0] = e.what()[0];
		return e.what()[0];
		//return returnString;
	}
	return VALID_MOVE;
	//return returnString;
}
