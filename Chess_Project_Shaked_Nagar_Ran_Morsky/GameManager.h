#pragma once
#include <iostream>
#include "Board.h"

#define PLAYER_MADE_CHESS '1'
#define VALID_MOVE '0'

class GameManager
{
public:
	//methods
	//GameMannager();
	//~GameMannager();

	GameManager();
	~GameManager();

	char turnHandler(std::string move);
private:
	//fields
	int _turnCounter;
	bool _currentPlayer;
	Board _board;
	Board _oldBoard;
};

