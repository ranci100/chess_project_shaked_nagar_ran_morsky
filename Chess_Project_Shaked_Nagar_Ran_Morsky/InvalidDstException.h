#include <iostream>

class InvalidDstException : public std::exception
{
public:
	virtual const char* what() const ;
};