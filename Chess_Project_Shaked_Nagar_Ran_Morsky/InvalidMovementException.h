#include <iostream>

class InvalidMovementException : public std::exception
{
public:
	virtual const char* what() const;
};