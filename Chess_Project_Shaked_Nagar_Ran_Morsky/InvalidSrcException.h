#include <iostream>

class InvalidSrcException : public std::exception
{
public:
	virtual const char* what() const;
};