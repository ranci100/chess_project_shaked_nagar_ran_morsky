#include "King.h"


King::King(int x, int y, bool color) 
	:Piece(x, y, color, TYPE_OF_KING)
{
	this->_firstMove = true;
}

King::~King() 
{
	
}

/*
	This function checks whether or not a movement to the given destination is valid
*/
bool King::checkMove(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)const 
{
	bool validMove = false;

	validMove = (abs(this->_x - dst.getX()) <= 1 && abs(this->_y - dst.getY()) <= 1 && (abs(this->_x - dst.getX()) + abs(this->_y - dst.getY())) > 0);
	
	//validMove = validMove || this->checkCastling(pieces, dst);

	return validMove;
}

/*
	This function returns whether or not a castling is the move given in the dst and this object
	We decided not to allow castling at this moment
*/
bool King::checkCastling(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)const
{
	int x = this->_x;
	unsigned int i = 0;
	
	if (this->_firstMove && this->_y == dst.getY())//checks if the king already moved
	{
		if (this->_x > dst.getX()) 
		{
			x = 0;
		}
		else 
		{
			x = 7;
		}
		//if(pieces[x][this->_y]->getType() == TYPE_OF_ROOK && (Rook)pieces[x][this->_y].isFirstMove())
		for (i = 0; i < abs(this->_x - dst.getX()); i++) 
		{
			x++;
			//if (pieces[x][this->_y]->getType() != TYPE_OF_EMPTY || isThreating()) 
			//{
				
			//}
		}
	}
	return false;
}

