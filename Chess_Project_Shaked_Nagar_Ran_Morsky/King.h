#include "Piece.h"


class King : public Piece 
{
public:
	//methods
	
	King(int x, int y, bool color);
	~King();
	virtual bool checkMove(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece & dst)const;
	bool checkCastling(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)const;


private:
	//fields
	bool _firstMove;

};