#include "Knight.h"

/*
	This is the constructor of the Knight object
*/
Knight::Knight(int x, int y, bool color)
	: Piece(x, y, color, TYPE_OF_KNIGHT)
{
	
}
/*
	This is the destructor of the Knight object
*/
Knight::~Knight() 
{
	
}

/*
	This function gets a 2d array of pieces, and a destination piece.
	The function returns whether or not the Knight can move to the destination location
*/
bool Knight::checkMove(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)const 
{
	int xGap = abs(this->_x - dst.getX());
	int yGap = abs(this->_y - dst.getY());
	return ((xGap + yGap) == 3 && (xGap == 1 || yGap == 1));
}