#include "Piece.h"


class Knight : public Piece 
{
public:
	Knight(int x, int y, bool color);

	~Knight();

	virtual bool checkMove(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)const;
};



