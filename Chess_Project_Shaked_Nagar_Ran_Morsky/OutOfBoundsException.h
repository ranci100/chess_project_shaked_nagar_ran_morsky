#include <iostream>

class OutOfBoundsException : public std::exception
{
public:
	virtual const char* what() const;
};