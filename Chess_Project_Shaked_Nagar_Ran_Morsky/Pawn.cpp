#include "Pawn.h"

/*
	This is the constructor of the Pawn
*/
Pawn::Pawn(int x, int y, bool color)
	: Piece(x, y, color, TYPE_OF_PAWN)
{

}

/*
	This is the destructor of the pawn.
*/
Pawn::~Pawn()
{

}

/*
	This function returns whether or not the Pawn can move to the given location.
	The function seperates the movement of the pawn to two parts: eating and going streight.
	The function checks each part in a different part of the code.
*/
bool Pawn::checkMove(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)const
{
	bool validMove = false;
	if (this->_x == dst.getX())//checks a case of going streight
	{
		if (abs(this->_y - dst.getY()) <= 2)//checks if the pawn goes up to 2 squares forward
		{
			if (this->_color && (this->_y - dst.getY()) > 0)//checks if the pawn is white and going forward
			{
				validMove = true;
			}
			else if (!this->_color && (dst.getY() - this->_y) > 0)//checks if the pawn is black and going forward
			{
				validMove = true;
			}
			validMove = validMove && dst.getType() == TYPE_OF_EMPTY;//checks if the pawn's destination is enmpty

			if (abs(this->_y - dst.getY()) == 2)//checks if the pawn goes 2 squares forward
			{
				//checks if the square at the 1 step forward from the pawn is empty
				validMove = validMove && (pieces[(this->_y + dst.getY()) HALF][this->_x]->getType() == TYPE_OF_EMPTY);//problem with this line
				if (!((this->_color && this->_y == STARTING_WHITE_SQUARE) || (!this->_color && this->_y == STARTING_BLACK_SQUARE)))
					//checks if this is the first move of the pawn.
				{
					validMove = false;
				}
			}
		}
	}
	else if (abs(this->_x - dst.getX()) == 1 && abs(this->_y - dst.getY()) == 1)//checks a case of eating
	{
		if (this->_color && (this->_x - dst.getX()) > 0 && dst.getType() != "Empty" && this->_color != dst.getColor())//white, valid eating
		{
			validMove = true;
		}
		else if (!this->_color && (this->_x - dst.getX()) < 0 && dst.getType() != "Empty" && this->_color != dst.getColor())//black, valid eating
		{
			validMove = true;
		}
	}
	return validMove;
}

/*
	This function changes the pawn into another Piece.
	The function is empty right now because we need to change the gui in order to perform this function.
*/
void Pawn::enPassent(Piece& src)
{

}

/*
	Thiss function checks whether or not the pawn arrived to the end of the board and should be promote to another Piece
*/
bool Pawn::checkEnPassent(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)const
{
	bool validMove = false;

	validMove = this->_color && this->_y == PRE_ENPASSENT_WHITE_SQUARE;//checks a case of a white pawn

	validMove = validMove || (!this->_color && this->_y == PRE_ENPASSENT_BLACK_SQUARE);// checks a case of a black pawn

	validMove = validMove && dst.getType() == TYPE_OF_EMPTY;
	return validMove;
}