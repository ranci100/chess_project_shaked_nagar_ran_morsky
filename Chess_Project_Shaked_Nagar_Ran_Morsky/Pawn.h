#include "Piece.h"

#define PRE_ENPASSENT_WHITE_SQUARE 1
#define PRE_ENPASSENT_BLACK_SQUARE 6

#define STARTING_WHITE_SQUARE 6
#define STARTING_BLACK_SQUARE 1

#define HALF /2

class Pawn : public Piece
{
public:
	Pawn(int x, int y, bool color);

	~Pawn();

	virtual bool checkMove(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)const;



	void enPassent(Piece& src);

private:
	bool checkEnPassent(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)const;

};