#include "Piece.h"

//the constructor init the variables of Piece
Piece::Piece(int x, int y, bool color, std::string type)
{
	this->_x = x;
	this->_y = y;
	this->_color = color;
	this->_type = type;
}

Piece::~Piece()
{
}

//the function return the x of the piece
int Piece::getX()const
{
	return this->_x;
}

//the function return the y of the piece
int Piece::getY()const
{
	return this->_y;
}

//the function get new x and set the y as variable in Piece
void Piece::setX(int x)
{
	this->_x = x;
}

//the function get new y and set the y as variable in Piece
void Piece::setY(int y)
{
	this->_y = y;
}

//the function return the type of the Piece
std::string Piece::getType() const
{
	return this->_type;
}

//the function return the color of the Piece
bool Piece::getColor() const
{
	return this->_color;
}