#pragma once
#include <iostream>
#include "InvalidDstException.h"
#include "InvalidMovementException.h"
#include "InvalidSrcException.h"
#include "OutOfBoundsException.h"
#include "SrcAndDstAreSameException.h"
#include "UnconventionalMovementException.h"
#include <cstdlib>

#define BOARD_SIZE 8
#define BLACK_KNIGHT 'n'
#define BLACK_PAWN 'p'
#define EMPTY '#'
#define BLACK_BISHOP 'b'
#define BLACK_ROOK 'r'
#define BLACK_QUEEN 'q'
#define BLACK_KING 'k'

#define WHITE_KNIGHT 'N'
#define WHITE_PAWN 'P'
#define WHITE_BISHOP 'B'
#define WHITE_ROOK 'R'
#define WHITE_QUEEN 'Q'
#define WHITE_KING 'K'

#define TYPE_OF_EMPTY "Empty"
#define TYPE_OF_PAWN "Pawn"
#define TYPE_OF_BISHOP "Bishop"
#define TYPE_OF_KNIGHT "Knight"
#define TYPE_OF_ROOK "Rook"
#define TYPE_OF_QUEEN "Queen"
#define TYPE_OF_KING "King"



class Piece
{
public:
	//the constructor init the variables of Piece
	Piece(int x, int y, bool color, std::string type);
	~Piece();
	//the function return the type of the Piece
	std::string getType() const;
	//the function set new type to the piece
	void setType(char newType);
	//the function return the color of the Piece
	bool getColor() const;
	virtual bool checkMove(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)const = 0;
	//the function return the x of the piece
	int getX()const;
	//the function return the y of the piece
	int getY()const;
	//the function get new x and set the y as variable in Piece
	void setX(int x);
	//the function get new y and set the y as variable in Piece
	void setY(int y);

protected:
	int _x;
	int _y;
	bool _color;
	std::string _type;
};