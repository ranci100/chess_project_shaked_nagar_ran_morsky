#include "Queen.h"

Queen::Queen(int x, int y, bool color) :Bishop(x, y, color), Rook(x, y, color), Piece(x, y, color, TYPE_OF_QUEEN)
{

}

Queen::~Queen()
{

}

bool Queen::checkMove(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)const
{
	return this->checkDiagonalMove(pieces, dst) || this->checkPath(pieces, dst);
}