#pragma once

#include "Piece.h"
#include "Rook.h"
#include "Bishop.h"

class Queen : public Bishop, public Rook, public Piece
{
public:
	//
	Queen(int x, int y, bool color);

	~Queen();

	virtual bool checkMove(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)const;

private:


};