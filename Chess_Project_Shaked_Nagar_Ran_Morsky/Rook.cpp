#include "Rook.h"

Rook::Rook(int x, int y, bool color) :Piece(x, y, color, TYPE_OF_ROOK)
{
	this->_firstMove = true;
}

Rook::~Rook()
{

}

bool Rook::checkMove(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)const
{
	return checkPath(pieces, dst);
}

bool Rook::checkPath(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)const
{
	int x = this->_x;
	int y = this->_y;

	if (this->_x == dst.getX())//checking the direction
	{
		if (this->_y > dst.getY())
		{
			y = dst.getY();
		}
		return checkStraightMove(pieces, dst, x, y, true);
	}
	else if (this->_y == dst.getY())//checking the direction
	{
		if (this->_x > dst.getX())
		{
			x = dst.getX();
		}
		return checkStraightMove(pieces, dst, x, y, false);

	}

	return false;
}

//the function go on every piece on the way and check if it empty
bool Rook::checkStraightMove(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst, int& x, int& y, bool direction) const
{
	int i = 0;

	for (i; i < abs(this->_y - dst.getY()) - 1; i++)
	{
		if (direction) //checking the direction 
		{
			y++;
		}
		else
		{
			x++;
		}
		if (pieces[y][x]->getType() != TYPE_OF_EMPTY) //check if someone on the way
		{
			return false;
		}
	}
	return true;
}

bool Rook::isFirstMove()const
{
	return this->_firstMove;
}
//the function set the private variable as false
bool Rook::setFirstMove(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)
{
	if (checkMove(pieces, dst))
	{
		this->_firstMove = false;
		return true;
	}
	return false;
}
