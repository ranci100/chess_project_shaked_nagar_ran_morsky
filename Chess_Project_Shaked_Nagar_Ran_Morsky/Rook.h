#pragma once
#include "Piece.h"
#include "Piece.h"

class Rook :public Piece
{
public:
	Rook(int x, int y, bool color);

	~Rook();

	virtual bool checkMove(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)const;

	bool isFirstMove()const;

protected:
	bool checkPath(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst)const;
private:
	bool checkStraightMove(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst, int& x, int& y, bool direction) const;
	bool setFirstMove(Piece* pieces[BOARD_SIZE][BOARD_SIZE], Piece& dst);
	bool _firstMove;
};
