#include <iostream>

class SrcAndDstAreSameException : public std::exception
{
public:
	virtual const char* what() const;
};