#include <iostream>

class UnconventionalMovementException : public std::exception
{
public:
	virtual const char* what() const;
};